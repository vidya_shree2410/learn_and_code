import java.util.*;

class MonkAndChamberOfSecrets {
        
	public static void main(String args[] )  {
		Scanner s = new Scanner(System.in);
		int TotalSpiders= s.nextInt();
		int spiderPower[] = new int[TotalSpiders+1];
		int numOfSpiderSelect = s.nextInt();
		int to=0;
		Queue<Integer> queueIndex = new LinkedList<Integer>();
		
		for(int i=1;i<TotalSpiders+1;i++){
			spiderPower[i] = s.nextInt();
			queueIndex.add(i);
		}
		
		
		for(int i=0;i<numOfSpiderSelect;i++){
			Queue<Integer> tempQueueIndex = new LinkedList<Integer>();
			if(numOfSpiderSelect < queueIndex.size())
			{
				to=numOfSpiderSelect;
			}
			else {
				to=queueIndex.size();
			}
			
			int maxIndex = queueIndex.remove();
			int maxPower = spiderPower[maxIndex];
			
			tempQueueIndex.add(maxIndex);
			
			for(int j = 1;j<to;j++){
				
				int tempIndex = queueIndex.remove();
				if(spiderPower[tempIndex] > maxPower){
					maxIndex = tempIndex;
					maxPower = spiderPower[tempIndex];
				}
				tempQueueIndex.add(tempIndex);
			}
			
			for(int j = 0;j<to;j++){
				int temp = tempQueueIndex.remove();
				if(temp != maxIndex){
					queueIndex.add(temp);

					if(spiderPower[temp] > 0) {
						spiderPower[temp]=(spiderPower[temp]-1) ;
					}
					else
					spiderPower[temp] =spiderPower[temp];
				}
			}
			
			System.out.print(maxIndex+" ");
		}
	}
}