package mypack;

import java.util.*;

class SpiderVariables{
	int spiderPower[];
	Queue<Integer> queueIndex;
	int tempIndex;
	int maxPower;
	void setIndexs(Queue<Integer> queueIndex,int tempIndex) {
		 
		this.queueIndex=queueIndex;
		this.tempIndex=tempIndex;
		
	}
	void setPowers(int maxPower) {
		this.maxPower=maxPower;
		
	}
}
public class RefactoredMonkAndChamberOfSecrets {

	static int[] readSpiderPower(int numOfSpiders) {
		int spiderPower[] = new int[numOfSpiders+1];
		Scanner s = new Scanner(System.in);
		for(int indexofSpider =1;indexofSpider <numOfSpiders+1;indexofSpider ++){
			spiderPower[indexofSpider ] = s.nextInt();
			
		}
		return spiderPower;
	}
	static Queue<Integer> storeQueueIndex(int numOfSpiders ) {
		Queue<Integer> queueIndex = new LinkedList<Integer>();
		for(int indexofSpider =1;indexofSpider <numOfSpiders+1;indexofSpider++){
			queueIndex.add(indexofSpider ); 
		}
		return queueIndex; 
	}
	static int findQueueSize(int numOfSpiderSelect,  Queue<Integer> queueIndex) {
		int queuesize=0;
		if(numOfSpiderSelect < queueIndex.size())
		{
			queuesize=numOfSpiderSelect;
		}
		else {
			queuesize=queueIndex.size();
		}
		return queuesize;
	}
	static int findMaxIndex(SpiderVariables spiderVariables,int maxIndex,int spiderPower[]){
		
		
		if(spiderPower[spiderVariables.tempIndex] >spiderVariables.maxPower){
			maxIndex = spiderVariables.tempIndex;
		
		}
		return maxIndex;
		}
	
static int findMaxpower(SpiderVariables spiderVariables,int spiderPower[]){
		
		
		if(spiderPower[spiderVariables.tempIndex] > spiderVariables.maxPower){
			
			spiderVariables.maxPower = spiderPower[spiderVariables.tempIndex];
		}
		return spiderVariables.maxPower;
		}
static int[] reducePower(int spiderPower[],int tempQueueIndexValue) {
	if(spiderPower[tempQueueIndexValue] > 0) {
		spiderPower[tempQueueIndexValue]=(spiderPower[tempQueueIndexValue]-1) ;
	}
	else
	spiderPower[tempQueueIndexValue] =spiderPower[tempQueueIndexValue];
	return spiderPower;
}
	 
	public static void main(String[] args )  {
		Scanner s = new Scanner(System.in);
		int numOfSpiders = s.nextInt();
		int spiderPower[] = new int[numOfSpiders+1];
		int numOfSpiderSelect = s.nextInt();
		int queuesize=0;
		Queue<Integer> queueIndex = new LinkedList<Integer>();
		SpiderVariables spiderVariables= new SpiderVariables() ;
		
		spiderPower=readSpiderPower(numOfSpiders);
		queueIndex=storeQueueIndex( numOfSpiders );
		
		for(int indexofSpider=0;indexofSpider<numOfSpiderSelect;indexofSpider++){
			Queue<Integer> tempQueueIndex = new LinkedList<Integer>();
			queuesize=findQueueSize(numOfSpiderSelect,  queueIndex);
			
			
			int maxIndex = queueIndex.remove();
			
			int maxPower = spiderPower[maxIndex];
			
			tempQueueIndex.add(maxIndex);
			
			for(int iteratorofQueue = 1;iteratorofQueue<queuesize;iteratorofQueue++){
				
				int tempIndex = queueIndex.remove();
				spiderVariables.setPowers(maxPower);
				spiderVariables.setIndexs(queueIndex,tempIndex);
				maxIndex=findMaxIndex(spiderVariables, maxIndex, spiderPower);
				
				
				maxPower=findMaxpower(spiderVariables, spiderPower);
				tempQueueIndex.add(tempIndex);
			}
			
			
			for(int iteratorofQueue = 0;iteratorofQueue<queuesize;iteratorofQueue++){
				int tempQueueIndexValue = tempQueueIndex.remove();
				
				if(tempQueueIndexValue != maxIndex){
					queueIndex.add(tempQueueIndexValue);
				
					spiderPower=reducePower( spiderPower, tempQueueIndexValue);
				}
			}
			
			System.out.print(maxIndex+" ");
		}
			
			
	}
}
