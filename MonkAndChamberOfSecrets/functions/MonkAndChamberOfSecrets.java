package mypack;

import java.util.*;

class MonkAndChamberOfSecrets {
	

static int findQueueSize(int numOfSpiderSelect,Queue<Integer> queueIndex ,int queuesize){
	if (numOfSpiderSelect < queueIndex.size()) {
		queuesize = numOfSpiderSelect;
	} else {
		queuesize = queueIndex.size();
	}
	return queuesize;
}
static int findMaxPower(int tempIndex, int spiderPower[],int maxIndex, int maxPower  ){
	
	if (spiderPower[tempIndex] > maxPower) {
		maxIndex = tempIndex;
		maxPower = spiderPower[tempIndex];
	}
	return maxPower;
}
static int[] reduceSpiderPower(int spiderPower[],int temp, int maxIndex,Queue<Integer> queueIndex){
	if (temp != maxIndex) {
		queueIndex.add(temp);

		if (spiderPower[temp] > 0) {
			spiderPower[temp] = (spiderPower[temp] - 1);
		} else
			spiderPower[temp] = spiderPower[temp];
	}
	return spiderPower;
}
	public static void main(String args[]) {
		Scanner s = new Scanner(System.in);
		int TotalSpiders = s.nextInt();
		int spiderPower[] = new int[TotalSpiders + 1];
		int numOfSpiderSelect = s.nextInt();
		int queuesize = 0;
		Queue<Integer> queueIndex = new LinkedList<Integer>();

		for (int IndexofSpider = 1; IndexofSpider < TotalSpiders + 1; IndexofSpider++) {
			spiderPower[IndexofSpider] = s.nextInt();
			queueIndex.add(IndexofSpider);
		}

		for (int IndexofSpider = 0; IndexofSpider < numOfSpiderSelect; IndexofSpider++) {
			Queue<Integer> tempQueueIndex = new LinkedList<Integer>();
			
			queuesize=findQueueSize( numOfSpiderSelect,queueIndex , queuesize);
			int maxIndex = queueIndex.remove();
			int maxPower = spiderPower[maxIndex];

			tempQueueIndex.add(maxIndex);

			for (int IteratorofQueue = 1; IteratorofQueue < queuesize; IteratorofQueue++) {
				
				int tempIndex = queueIndex.remove();
				
				maxPower=findMaxPower( tempIndex, spiderPower, maxIndex,  maxPower);
				tempQueueIndex.add(tempIndex);
			}

			for (int IteratorofQueue = 0; IteratorofQueue < queuesize; IteratorofQueue++) {
				int temp = tempQueueIndex.remove();
				spiderPower=reduceSpiderPower(spiderPower, temp,  maxIndex,queueIndex);
			}

			System.out.print(maxIndex + " ");
		}
	}
}