import fs = require("fs");
import { serializer } from "./serialisation";

export class DbActivities {
    public objectData;
    public existingData;
    public id = 0;
    public totalObjects;

    selectDBActivity(requiredObjectDetails) {
        let fileContent = this.readFile(requiredObjectDetails.type);
        this.existingData = serializer.deserializeToJSON(fileContent);
        switch (requiredObjectDetails.activity) {
            case "save": {
               return this.Save(requiredObjectDetails);
            }
            case "find": {
               return this.Find(requiredObjectDetails);
            }
            case "delete": {
               return this.Delete(requiredObjectDetails);
            }
            case "edit": {
               return this.Edit(requiredObjectDetails);
            }
            default: {
                console.log('Invalid activity');
            }
        }
    }
    Save(requiredObjectDetails) {
        this.objectData = requiredObjectDetails.data;
        this.getNextId();
        this.existingData.push(this.objectData);
        let serialisedData = serializer.serializeJSONData(this.existingData);
        this.writeDataIntoFile(serialisedData, requiredObjectDetails.type);
    }
    Find(requiredObjectDetails) {
        let objIndex = 0;
        let searchResults = [];
        while (objIndex < this.existingData.length) {
            if (requiredObjectDetails.selectedPropertyValue === this.existingData[objIndex][requiredObjectDetails.selectedProperty]) {
                searchResults[0] = this.existingData[objIndex];
                searchResults[1] = objIndex;
                return searchResults;
            } else {
                objIndex++;
                if (objIndex == this.existingData.length) {
                    searchResults[0] = 'Object Not Found';
                    searchResults[1] = '';
                    return searchResults;
                }
            }
        }
    }

    Delete(requiredObjectDetails) {
        let searchResults = this.Find(requiredObjectDetails);
        let matchedObjectIndex = searchResults[1];
        if (matchedObjectIndex !== '') {
            this.existingData.splice(matchedObjectIndex, 1);
            let serialisedData = serializer.serializeJSONData(this.existingData);
            this.writeDataIntoFile(serialisedData, requiredObjectDetails.type);
            console.log('Object deleted successfully!');
        } else {
            console.log('Object Not Found');
        }
    }

    Edit(requiredObjectDetails) {
        let searchResults = this.Find(requiredObjectDetails);
        let matchedObjectIndex = searchResults[1];
        if (matchedObjectIndex !== '') {
            this.existingData[matchedObjectIndex][requiredObjectDetails.selectedProperty] = requiredObjectDetails.newPropertyValue;
            let editedObject = this.existingData[matchedObjectIndex];
            this.existingData.splice(matchedObjectIndex, 1, editedObject);
            let serialisedData = serializer.serializeJSONData(this.existingData);
            this.writeDataIntoFile(serialisedData, requiredObjectDetails.type);
            console.log('Object Edited successfully!');
        } else {
            console.log('Object Not Found');
        }
    }
    readFile(requiredObjectType) {
        let fileContent = fs.readFileSync(requiredObjectType + '.txt');
        return fileContent;
    }
    getNextId() {
        if (this.existingData.length !== 0) {
            this.id = this.existingData[this.existingData.length - 1].Id;
        }
        this.objectData.Id = this.id + 1;
    }

    writeDataIntoFile(serialisedData, requiredObjectType) {
        fs.writeFileSync(requiredObjectType + '.txt', serialisedData);
        console.log('Data stored Successfully');
    }
}