import read = require("readline-sync");
import { DbActivities } from './DBActivities';
import { book } from "./BookClass";
import { student } from "./StudentClass";
import { filevalidation } from "./FileValidations";
import { requriedObjectDetails } from "./ObjectDetail";

let DBActivities = new DbActivities();

function storeDataToDB() {
    let requiredObjectType = read.question("choose the category:\n 1.student\n 2.book\n");
    requriedObjectDetails.type = updateObjectType(requiredObjectType);
    if (getObjectData(requriedObjectDetails.type)) {
    if (filevalidation.checkForFileExistance(requriedObjectDetails.type)) {
        requriedObjectDetails.activity = 'save';
        DBActivities.selectDBActivity(requriedObjectDetails);
    }
}
}

function getDataFromDB() {
    let requiredObjectType = read.question("choose the category:\n 1.student\n 2.book\n");
    requriedObjectDetails.type = updateObjectType(requiredObjectType);
    if (!filevalidation.checkForEmptyFile(requriedObjectDetails.type)) {
        requriedObjectDetails.selectedProperty = selectAndUpdateObjectProperty(requriedObjectDetails.type);
        requriedObjectDetails.selectedPropertyValue = read.question("Enter the value :\n");
        requriedObjectDetails.activity = 'find';
       let searchResults = DBActivities.selectDBActivity(requriedObjectDetails);
        let matchedObject = searchResults[0];
        if (matchedObject instanceof Object == true) {
            displayRequiredObjectValues(matchedObject);
            return true;
        } else {
            console.log('Object Not Found');
            return false;
        }
    }
}

function deleteDataFromDB() {
    let requiredObjectType = read.question("choose the category:\n 1.student\n 2.book\n");
    requriedObjectDetails.type = updateObjectType(requiredObjectType);
    if (!filevalidation.checkForEmptyFile(requriedObjectDetails.type)) {
        requriedObjectDetails.selectedProperty = selectAndUpdateObjectProperty(requriedObjectDetails.type);
        requriedObjectDetails.selectedPropertyValue = read.question("Enter the value :\n");
        requriedObjectDetails.activity = 'delete';
        DBActivities.selectDBActivity(requriedObjectDetails);
    }
}
function editDataInDB() {
    let requiredObjectType = read.question("choose the category:\n 1.student\n 2.book\n");
    requriedObjectDetails.type = updateObjectType(requiredObjectType);
    if (!filevalidation.checkForEmptyFile(requriedObjectDetails.type)) {
        requriedObjectDetails.selectedProperty = selectAndUpdateObjectProperty(requriedObjectDetails.type);
        requriedObjectDetails.selectedPropertyValue = read.question("Enter the present value :\n");
        requriedObjectDetails.newPropertyValue = read.question("Enter new value :\n");
        requriedObjectDetails.activity = 'edit';
        DBActivities.selectDBActivity(requriedObjectDetails);
    }
}

function selectAndUpdateObjectProperty(requiredObjectType) {
    switch (requiredObjectType) {
        case "student":
            return student.selectStudentProperty();
        case "book":
            return book.selectBookProperty();
        default:
            console.log("Invalid Object");
            break;
    }
}

function updateObjectType(requiredObjectType) {
    let objtype;
    switch (requiredObjectType) {
        case "1": objtype = 'student';
            return objtype;
        case "2": objtype = 'book';
            return objtype;
        default: return console.log('Invalid choice');

    }
}

function getObjectData(requiredObjectType) {
    switch (requiredObjectType) {
        case "student": {
            requriedObjectDetails.data = student.getStudentData();
            return true;
        }
        case "book": {
            requriedObjectDetails.data = book.getBookDetails();
            return true;
        }
        default: {
            console.log('Invalid input');
            return false;
        }
    }
}

function displayRequiredObjectValues(object) {
    let requiredObject = Object.keys(object).map(key => object[key]);
    let requiredObjectDetails = requiredObject.join("\n");
    console.log(requiredObjectDetails);
}

function main() {
    let userChoiceForDbOperation;
    while (1) {
        userChoiceForDbOperation = read.question("Enter the operation to perform:\n1.Store\n2.Retreive\n3.Delete\n4.Edit\n5.Exit\n");
        switch (userChoiceForDbOperation) {
            case "1": storeDataToDB();
                break;
            case "2": getDataFromDB();
                break;
            case "3": deleteDataFromDB();
                break;
            case "4": editDataInDB();
                break;
            case "5": return;
            default: console.log("Invalid choice of Operation");
        }
    }

}

main();