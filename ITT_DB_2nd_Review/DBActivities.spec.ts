import { DbActivities } from './DBActivities';
import { requriedObjectDetails } from "./ObjectDetail"
import { serializer } from './serialisation';

let dbActivities = new DbActivities();

describe('DbActivities:Class', () => {
    beforeEach(() => {
        requriedObjectDetails.type = 'book';
        let fileContent = this.readFile(requriedObjectDetails.type);
        let existingData = serializer.deserializeToJSON(fileContent);
        });
    it('should not save', () => {
        //Arrange
        requriedObjectDetails.type = 'customer';
        //Act
        let results = dbActivities.Save(requriedObjectDetails);
        //Assert
        expect(results).toBeFalsy();
    })
    it('should save book object', () => {
        //Arrange
        requriedObjectDetails.data = {
            'TitleOfTheBook' : 'Annana nenapu',
            'Author' : 'Poornachandra Tejaswi',
            'Language' : 'Kannada'
        }
        //Act
        let results = dbActivities.Save(requriedObjectDetails);
        //Assert
        expect(results).toBeTruthy();
    })
    it('should find book object', () => {
        //Arrange
        requriedObjectDetails.selectedProperty = 'TitleOfTheBook';
        requriedObjectDetails.selectedPropertyValue = 'Annana nenapu';
        //Act
        let results = dbActivities.Find(requriedObjectDetails);
        //Assert
        expect(results).toBeTruthy();
    })
    it('should not find student object', () => {
        //Arrange
        requriedObjectDetails.selectedProperty = 'TitleOfTheBook';
        requriedObjectDetails.selectedPropertyValue = 'Untethered Soul';
        //Act
        let results = dbActivities.Find(requriedObjectDetails);
        //Assert
        expect(results).toBeFalsy();
    })
    it('should delete book object', () => {
        //Arrange
        requriedObjectDetails.selectedProperty = 'TitleOfTheBook';
        requriedObjectDetails.selectedPropertyValue = 'Annana nenapu';
        //Act
        let results = dbActivities.Delete(requriedObjectDetails);
        //Assert
        expect(results).toBeTruthy();
    })

    it('should not delete book object', () => {
        //Arrange
        requriedObjectDetails.selectedProperty = 'TitleOfTheBook';
        requriedObjectDetails.selectedPropertyValue = 'Untethered Soul';
        //Act
        let results = dbActivities.Delete(requriedObjectDetails);
        //Assert
        expect(results).toBeFalsy();
    })
    it('should edit book object', () => {
        //Arrange
        requriedObjectDetails.selectedProperty = 'TitleOfTheBook';
        requriedObjectDetails.selectedPropertyValue = 'Magic of Thinking Big';
        requriedObjectDetails.newPropertyValue = 'The Magic of Thinking Big';
        //Act
        let results = dbActivities.Edit(requriedObjectDetails);
        //Assert
        expect(results).toBeTruthy();
    })
    it('should not edit book object', () => {
        //Arrange
        requriedObjectDetails.selectedProperty = 'TitleOfTheBook';
        requriedObjectDetails.selectedPropertyValue = 'Magic Book';
        requriedObjectDetails.newPropertyValue = 'The Magic of Thinking Big';
        //Act
        let results = dbActivities.Edit(requriedObjectDetails);
        //Assert
        expect(results).toBeFalsy();
    })

})