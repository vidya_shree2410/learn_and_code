export class ObjectDetails {
    type: string;
    selectedProperty: string;
    selectedPropertyValue: string;
    newPropertyValue: string;
    activity: string;
    data: Object;
}

export let requriedObjectDetails = new ObjectDetails;