import read = require("./node_modules/readline-sync");

export class Students {
    Id: string;
    Name: string;
    Branch: string;
    getStudentData: () => object;
    selectStudentProperty: () => string;
}

export let student = new Students;

Students.prototype.getStudentData = (): object => {
    student.Name = read.question('Enter Student Name: ');
    student.Branch = read.question('Enter Branch: ');
    return student;  
}

Students.prototype.selectStudentProperty = (): string => {
    let selectedoption = read.question('Please select the property :\n 1.Name \n 2.Branch \n');
    switch (selectedoption) {
        case "1": return 'Name';
        case "2": return 'Branch';
        default: 
        console.log("Invalid property selection");
        return;
    }
}