
export class Serialize {
    serializeJSONData(data){
        let serialisedData = JSON.stringify(data);
        return serialisedData; 
    }
    deserializeToJSON(fileContent) {
        let deserializedData = JSON.parse(fileContent.toString());
        return deserializedData;
    }
}
export let serializer = new Serialize();