import fs = require("fs");

export class FileValidations {
    checkForEmptyFile(requiredObjectType) {
        let existingData = fs.readFileSync(requiredObjectType + '.txt').toString();
        try {
            if (existingData.length == 0) {
                throw new Error('File is Empty')
            } else {
                return false;
            }
        }
        catch (e) {
            console.log('Error : ' + e.message);
            return true;
        }
    }
    checkForFileExistance(requiredObjectType) {
        try {
            if (!fs.existsSync(requiredObjectType + '.txt')) {
                throw new Error('File does not Exists');
            } else {
                return true;
            }
        }
        catch (e) {
            console.log(e.message);
            return false;
        }
    }
}

export let filevalidation = new FileValidations;