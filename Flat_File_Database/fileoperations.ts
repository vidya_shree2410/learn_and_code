import fs = require("fs");
import { book } from "./BookClass";
import { student} from "./StudentClass";

export class fileoperations {
    public objectdata;
    public existingdata;

    Save(objtype) {
        this.getdata(objtype);  
        if(this.checkForDuplicateId(objtype, this.objectdata)) {
            console.log('This Id already exists!')
        } else {
        this.storedata(this.objectdata, objtype);
        }
    }

    getdata(objtype) {
        switch (objtype) {
            case "student": {
                this.objectdata = student.getstudentdata();
                break;
            }
            case "book": {
                this.objectdata = book.getbookdetails();
                break;
            }
            default: {
                console.log('Invalid input');
                break;
            }
        }
        return this.objectdata;
    }

    storedata(objectdata, objtype) {
        let objdata = JSON.stringify(objectdata);
        fs.appendFileSync(objtype + ".txt", objdata);
        this.ConvertAndStoreDataInJsonForm(objtype);
    }

    checkForDuplicateId(objtype, objectdata) {
        this.existingdata = fs.readFileSync(objtype + '.txt').toString();
        this.convertDataIntoStringForm();
        return this.SearchForDuplicateId(objectdata);
    }

    SearchForDuplicateId(objectdata) {
        let objIndex = 0;
        while (objIndex < this.existingdata.length) {
            let object = JSON.parse(this.existingdata[objIndex]);
            if (objectdata.Id === object.Id) {
                return true;
            } else {
                objIndex++;
                if (objIndex == this.existingdata.length) {
                    return false;
                }
            }
        }
    }

    ConvertAndStoreDataInJsonForm(objtype) {
        this.existingdata = fs.readFileSync(objtype + '.txt').toString();
        this.existingdata = this.existingdata.replace(/\[/g, '');
        this.existingdata = this.existingdata.replace(/\]/g, '');
        this.existingdata = this.existingdata.replace(/\}\{/g, '},{');
        this.existingdata = '['.concat(this.existingdata + ']');
        JSON.stringify(this.existingdata);
        fs.writeFileSync(objtype + '.txt', this.existingdata);
        console.log('Data stored Successfully.');
    }


    Find(objtype, selectedproperty, selectedpropertyvalue) {
        this.existingdata = fs.readFileSync(objtype + '.txt').toString();
        this.convertDataIntoStringForm();
        this.searchAndDisplayRequiredObject(selectedproperty, selectedpropertyvalue);
    }

    convertDataIntoStringForm() {
        this.existingdata = this.existingdata.replace(/\[/g, '');
        this.existingdata = this.existingdata.replace(/\]/g, '');
        this.existingdata = this.existingdata.replace(/\}\,\{/g, '}\n{');
        this.existingdata = this.existingdata.split('\n');
    }

    searchAndDisplayRequiredObject(selectedproperty, selectedpropertyvalue) {
        let objIndex = 0;
        while (objIndex < this.existingdata.length) {
            let object = JSON.parse(this.existingdata[objIndex]);
            if (selectedpropertyvalue === object[selectedproperty]) {
                return this.displayRequiredObjectValues(object);
            } else {
                objIndex++;
                if (objIndex == this.existingdata.length) {
                    return console.log('Object Not Found');
                }
            }
        }
    }

    displayRequiredObjectValues(object) {
        let requiredObject = Object.keys(object).map(key => object[key]);
        let requiredObjectDetails = requiredObject.join("\n");
        console.log(requiredObjectDetails);
    }

}