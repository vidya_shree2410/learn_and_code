import read = require("./node_modules/readline-sync");

export class Books {
    Id: number;
    TitleOfTheBook: string;
    Author: string;
    Language: string;
    getbookdetails: () => object;
    selectbookproperty: () => string;
}

export let book = new Books;

Books.prototype.getbookdetails = (): object => {
    book.Id = read.question('Enter Book Id: ');
    book.TitleOfTheBook = read.question('Enter title of the book: ');
    book.Author = read.question('Enter author name: ');
    book.Language = read.question('Enter language: ');
    return book;  
}

Books.prototype.selectbookproperty = (): string => {
    let selectedproperty = read.question('Please select the property :\n Id \n TitleOfTheBook \n Author \n Language \n');
    return selectedproperty;  
}