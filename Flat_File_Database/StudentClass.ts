import read = require("./node_modules/readline-sync");

export class Students {
    Id: number;
    Name: string;
    Branch: string;
    getstudentdata: () => object;
    selectstudentproperty: () => string;
}

export let student = new Students;

Students.prototype.getstudentdata = (): object => {
    student.Id = read.question('Enter Student Id: ');
    student.Name = read.question('Enter Student Name: ');
    student.Branch = read.question('Enter Branch: ');
    return student;  
}

Students.prototype.selectstudentproperty = (): string => {
    let selectedproperty = read.question('Please select the property :\n Id \n Name \n Branch \n');
    return selectedproperty;
}