import read = require("./node_modules/readline-sync");
import fs = require("fs");
import { fileoperations } from './fileoperations';
import { book } from "./BookClass";
import { student } from "./StudentClass";
import {exception} from "./ITTDBExceptions"

function storeDataToDb() {
    let continueRequest;
    let fileoperation = new fileoperations();
    do {
        let objType = read.question("choose the category:\n student\n book\n");
        if(exception.CheckForFileExistance(objType)){
        fileoperation.Save(objType);
        }
        continueRequest = read.question("Do you want to continue?yes or no\n");
    } while (continueRequest == "yes");
}
function retreiveDataFromDb() {
    let fileoperation = new fileoperations();
    let objtype = read.question("choose the category:\n 1.student\n 2.book\n");
    if (!exception.CheckForEmptyFile(objtype)) {
        let selectedproperty = selectAndUpdateObjectProperty(objtype);
        let selectedpropertyvalue = read.question("Enter the value :\n");
        fileoperation.Find(objtype, selectedproperty, selectedpropertyvalue);
    }
}


function selectAndUpdateObjectProperty(objtype) {
    switch (objtype) {
        case "student":
            return student.selectstudentproperty();
        case "book":
            return book.selectbookproperty();
        default:
            console.log("Invalid Object");
            break;
    }
}

function main() {
    let userChoiceForDbOperation;
    userChoiceForDbOperation = read.question("Enter the operation to perform:\n1.Store\n2.Retreive\n");
    switch (userChoiceForDbOperation) {
        case "1": storeDataToDb();
            break;
        case "2": retreiveDataFromDb();
            break;
        default: console.log("Invalid choice of Operation");
            break;
    }

}

main();