
export class ObjectManager {
    serializeJSONData(data: any[] | Object[]) {
        let serialisedData = JSON.stringify(data);
        return serialisedData;
    }
    deserializeToJSON(fileContent: Buffer) {
        let deserializedData = JSON.parse(fileContent.toString());
        return deserializedData;
    }
}
export let objectManager = new ObjectManager();