
export class Request {
    Type: string;
    data?: RequestData;
    fileName: string;
}

export class RequestData {
    data?: object;
    selectedProperty?: string;
    selectedPropertyValue?: string | number;
    selectedPropertyNewValue?: string | number;

}

