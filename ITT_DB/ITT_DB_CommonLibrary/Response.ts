export class Response {
    success: boolean;
    message: string;
    data?: ResponseData;
}
export class ResponseData {
    requiredObject: object;
    requiredObjectIndex: number;
}
export class SearchResults {
    requiredObject: object;
    requiredObjectIndex: number;
}