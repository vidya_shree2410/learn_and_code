
export class Students {
    Id: string;
    Name: string;
    Branch: string;
    getStudentData: () => object;
    selectStudentProperty: () => string;
}

export let student = new Students;

Students.prototype.getStudentData = (): object => {
    student.Name = (document.getElementById('Name') as HTMLFormElement).value;
    student.Branch = (document.getElementById('Branch') as HTMLFormElement).value;
    return student;
}

Students.prototype.selectStudentProperty = (): string => {
    let selectedoption = (document.getElementById('selectStudentProperty') as HTMLFormElement).value;
    switch (selectedoption) {
        case "1": return 'Name';
        case "2": return 'Branch';
        default:
            alert("Invalid property selection");
            return;
    }
}