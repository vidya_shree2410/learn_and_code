import { Request } from "../ITT_DB_CommonLibrary/Request";
import { Response } from '../ITT_DB_CommonLibrary/Response';
import { connectionHandler } from './ClientConnectionHandler';
import { client_helper } from './Client_helper';

export class RequestHandler {
    async Save(objectType, objectDetails) {
        let request: Request = {
            Type: 'save',
            fileName: objectType,
            data: {
                data: objectDetails
            }
        }
        connectionHandler.sendRequest(request);
        try {
            let results: Response = await connectionHandler.recieveResponse();
            console.log(results.message);
        }

        catch (err) {
            return "Error: " + err
        }
    }
    async Find(objectType, selectedProperty, selectedPropertyValue) {
        let request: Request = {
            Type: 'find',
            fileName: objectType,
            data: {
                selectedProperty: selectedProperty,
                selectedPropertyValue: selectedPropertyValue
            }
        }
        connectionHandler.sendRequest(request);
        try {
            let results: Response = await connectionHandler.recieveResponse();
            console.log(results.message);
            if (results.data.requiredObject instanceof Object == true) {
                client_helper.displayRequiredObjectValues(results.data.requiredObject);
            }
        }
        catch (err) {
            return "Error: " + err
        }
    }
    async Delete(objectType, selectedProperty, selectedPropertyValue) {
        let request: Request = {
            Type: 'delete',
            fileName: objectType,
            data: {
                selectedProperty: selectedProperty,
                selectedPropertyValue: selectedPropertyValue
            }
        }
        connectionHandler.sendRequest(request);
        try {
            let results: Response = await connectionHandler.recieveResponse();
            console.log(results.message);
        }
        catch (err) {
            return "Error: " + err
        }
    }
    async Edit(objectType, selectedProperty, selectedPropertyValue, selectedPropertyNewValue) {
        let request: Request = {
            Type: 'update',
            fileName: objectType,
            data: {
                selectedProperty: selectedProperty,
                selectedPropertyValue: selectedPropertyValue,
                selectedPropertyNewValue: selectedPropertyNewValue
            }
        }
        connectionHandler.sendRequest(request);
        try {
            let results: Response = await connectionHandler.recieveResponse();
            console.log(results.message);
        }
        catch (err) {
            return "Error: " + err
        }
    }

}

export let requestHandler = new RequestHandler;