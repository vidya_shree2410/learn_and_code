let userChoiceForDbOperation = '';
let objectType = '';


function DbActivity() {
  userChoiceForDbOperation = (document.getElementById('fileOperation') as HTMLFormElement).value;
  if (userChoiceForDbOperation !== '') {
    (document.getElementById('displayObjectType') as HTMLElement).style.display = 'block';
  } else {
    (document.getElementById('displayObjectType') as HTMLElement).style.display = "none";
  }
  selectObject();
}
function displayObjectForm() {
  if (userChoiceForDbOperation === 'save' && objectType === 'student') {
    (document.getElementById('studentForm') as HTMLElement).style.display = 'block';
  } else {
    (document.getElementById('studentForm') as HTMLElement).style.display = "none";
  }
  if (userChoiceForDbOperation === 'save' && objectType === 'book') {
    (document.getElementById('bookForm') as HTMLElement).style.display = 'block';
  } else {
    (document.getElementById('bookForm') as HTMLElement).style.display = "none";
  }
}

function displayDbActionForBookObject() {
  if (userChoiceForDbOperation !== 'save' && objectType === 'book') {
    (document.getElementById('bookDetails') as HTMLElement).style.display = 'block';
    if (userChoiceForDbOperation === 'delete') {
      (document.getElementById('deleteBook') as HTMLElement).style.display = 'block';
    } else {
      (document.getElementById('deleteBook') as HTMLElement).style.display = 'none';
    }
    if (userChoiceForDbOperation === 'find') {
      (document.getElementById('findBook') as HTMLElement).style.display = 'block';
    } else {
      (document.getElementById('findBook') as HTMLElement).style.display = 'none';
    }
    if (userChoiceForDbOperation === 'update') {
      (document.getElementById('updateBook') as HTMLElement).style.display = 'block';
    } else {
      (document.getElementById('updateBook') as HTMLElement).style.display = 'none';
    }
  } else {
    (document.getElementById('bookDetails') as HTMLElement).style.display = 'none';
  }
}
function displayDbActionForStudentObject() {
  if (userChoiceForDbOperation !== 'save' && objectType === 'student') {
    (document.getElementById('studentDetails') as HTMLElement).style.display = 'block';
    if (userChoiceForDbOperation === 'delete') {
      (document.getElementById('deleteStudent') as HTMLElement).style.display = 'block';
    } else {
      (document.getElementById('deleteStudent') as HTMLElement).style.display = 'none';
    }
    if (userChoiceForDbOperation === 'find') {
      (document.getElementById('findStudent') as HTMLElement).style.display = 'block';
    } else {
      (document.getElementById('findStudent') as HTMLElement).style.display = 'none';
    }
    if (userChoiceForDbOperation === 'update') {
      (document.getElementById('updateStudent') as HTMLElement).style.display = 'block';
    } else {
      (document.getElementById('updateStudent') as HTMLElement).style.display = 'none';
    }
  } else {
    (document.getElementById('studentDetails') as HTMLElement).style.display = 'none';
  }
}
function selectObject() {
  objectType = (document.getElementById('objectType') as HTMLFormElement).value;
  displayObjectForm();
  displayDbActionForBookObject();
  displayDbActionForStudentObject();
}
function main() {
  (document.getElementById('studentForm') as HTMLElement).style.display = 'none';
  (document.getElementById('bookForm') as HTMLElement).style.display = 'none';
  (document.getElementById('displayObjectType') as HTMLElement).style.display = 'none';
  (document.getElementById('bookDetails') as HTMLElement).style.display = 'none';
  (document.getElementById('studentDetails') as HTMLElement).style.display = 'none';
}