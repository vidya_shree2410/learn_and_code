import { book } from "./BookClass";
import { student } from "./StudentClass";

export class Client_helper {
    public selectedPropertyValue;
    public selectedPropertyNewValue;
    public objectDetails;
    getObjectData(objectType) {
        switch (objectType) {
            case "student": {
                this.objectDetails = student.getStudentData();
                return this.objectDetails;
            }
            case "book": {
                this.objectDetails = book.getBookDetails();
                return this.objectDetails;
            }
            default: {
                alert('Invalid input');
                return '';
            }
        }
    }
    selectAndUpdateObjectProperty(objectType) {
        switch (objectType) {
            case "student":
                return student.selectStudentProperty();
            case "book":
                return book.selectBookProperty();
            default:
                alert("Invalid Object");
                break;
        }
    }

    getSelectedPropertyValue(objectType) {
        switch (objectType) {
            case "student":
                return this.selectedPropertyValue = (document.getElementById('selectedStudentPropertyValue') as HTMLFormElement).value;
            case "book":
                return this.selectedPropertyValue = (document.getElementById('selectedBookPropertyValue') as HTMLFormElement).value;
            default:
                alert("Invalid Object");
                break;
        }
    }
    getSelectedPropertyNewValue(objectType) {
        switch (objectType) {
            case "student":
                return this.selectedPropertyNewValue = (document.getElementById('selectedStudentPropertyNewValue') as HTMLFormElement).value;
            case "book":
                return this.selectedPropertyNewValue = (document.getElementById('selectedBookPropertyNewValue') as HTMLFormElement).value;
            default:
                alert("Invalid Object");
                break;
        }
    }

    displayRequiredObjectValues(object) {
        let requiredObject = Object.keys(object).map(key => object[key]);
        let requiredObjectDetails = requiredObject.join("\n");
        console.log(requiredObjectDetails);
      }
}

export let client_helper = new Client_helper;