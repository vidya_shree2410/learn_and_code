
export class Books {
    Id: number;
    TitleOfTheBook: string;
    Author: string;
    Language: string;
    getBookDetails: () => object;
    selectBookProperty: () => string;
}

export let book = new Books;

Books.prototype.getBookDetails = (): object => {
    book.TitleOfTheBook = (document.getElementById('TitleOfTheBook') as HTMLFormElement).value;
    book.Author = (document.getElementById('Author') as HTMLFormElement).value;
    book.Language = (document.getElementById('Language') as HTMLFormElement).value;
    return book;
}

Books.prototype.selectBookProperty = (): string => {
    let selectedoption = (document.getElementById('selectBookProperty') as HTMLFormElement).value;
    switch (selectedoption) {
        case "1": return 'TitleOfTheBook';
        case "2": return 'Author';
        case "3": return 'Language';
        default:
            alert("Invalid property selection");
            return;
    }
}