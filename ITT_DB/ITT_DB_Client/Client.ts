import { connectionHandler } from './ClientConnectionHandler';
import { requestHandler } from './RequestHandler';
import { client_helper } from './Client_helper';

let objectType;

function storeDataToDB() {
  let objectDetails = client_helper.getObjectData(objectType);
  requestHandler.Save(objectType, objectDetails);
}
function getDataFromDB() {
  let selectedProperty = client_helper.selectAndUpdateObjectProperty(objectType);
  let selectedPropertyValue = client_helper.getSelectedPropertyValue(objectType);
  requestHandler.Find(objectType, selectedProperty, selectedPropertyValue);
}
function deleteDataFromDB() {
  let selectedProperty = client_helper.selectAndUpdateObjectProperty(objectType);
  let selectedPropertyValue = client_helper.getSelectedPropertyValue(objectType);
  requestHandler.Delete(objectType, selectedProperty, selectedPropertyValue);
}
function editDataInDB() {
  let selectedProperty = client_helper.selectAndUpdateObjectProperty(objectType);
  let selectedPropertyValue = client_helper.getSelectedPropertyValue(objectType);
  let selectedPropertyNewValue = client_helper.getSelectedPropertyNewValue(objectType);
  requestHandler.Edit(objectType, selectedProperty, selectedPropertyValue, selectedPropertyNewValue)
}

connectionHandler.connect();