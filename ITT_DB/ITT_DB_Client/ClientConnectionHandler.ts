import { Response } from '../ITT_DB_CommonLibrary/Response';
import * as io from "socket.io";
import { objectManager } from '../ITT_DB_CommonLibrary/ObjectManager';

export class ConnectionHandler {
    public socket;
    sendRequest(request) {
        this.socket.emit("message", objectManager.serializeJSONData(request));
    }
    connect() {
        this.socket = io("http://localhost:3000");
    }
    recieveResponse(): Response {
        let response;
        this.socket.on("message", function (data) {
            response = objectManager.deserializeToJSON(data);
        });
        return response;
    }

}
export let connectionHandler = new ConnectionHandler;