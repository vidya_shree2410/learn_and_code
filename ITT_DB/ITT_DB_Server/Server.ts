import * as express from "express";
import * as path from "path";
import { connectionHandler } from './ServerConnectionHandler';

const app = express();

app.get("/", (req: any, res: any) => {
  res.sendFile(path.resolve("./ITT_DB_Client/userInterface.html"));
  connectionHandler.accept();
});

function server() {
  connectionHandler.startServer();
}

server();