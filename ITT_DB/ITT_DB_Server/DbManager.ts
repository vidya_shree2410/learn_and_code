import { objectManager } from "../ITT_DB_CommonLibrary/ObjectManager";
import { fileManager } from "./FileManager";
import { connectionHandler } from './ServerConnectionHandler';
import { SearchResults } from '../ITT_DB_CommonLibrary/Response';

export class DbManager {
    public objectData: Object;
    public existingData: any[] | Object[];
    public id = 0;

    Save(objectType: string, objectData: Object) {
        this.existingData = fileManager.getData(objectType);
        this.objectData = objectData;
        this.objectData.id = fileManager.getNextId();
        this.existingData.push(this.objectData);
        let serialisedData = objectManager.serializeJSONData(this.existingData);
        let results = fileManager.writeDataIntoFile(serialisedData, objectType);
        return connectionHandler.createResponse(results.success, results.message);
    }
    Find(objectType: any, selectedProperty: string | number, selectedPropertyValue: any) {
        this.existingData = fileManager.getData(objectType);
        let objIndex = 0;
        let searchResults: SearchResults;
        while (objIndex < this.existingData.length) {
            if (selectedPropertyValue === this.existingData[objIndex][selectedProperty]) {
                searchResults.requiredObject = this.existingData[objIndex];
                searchResults.requiredObjectIndex = objIndex;
                return connectionHandler.createResponse(true, 'Object Found', searchResults);
            } else {
                objIndex++;
                if (objIndex == this.existingData.length) {
                    searchResults.requiredObject = {};
                    searchResults.requiredObjectIndex = null;
                    return connectionHandler.createResponse(false, 'Object Not Found', searchResults);
                }
            }
        }
    }

    Delete(objectType: any, selectedProperty: string | number, selectedPropertyValue: any) {
        let results = this.Find(objectType, selectedProperty, selectedPropertyValue);
        let searchResults: any = results.data;
        let matchedObjectIndex = searchResults.requiredObjectIndex;
        if (matchedObjectIndex !== null) {
            this.existingData.splice(matchedObjectIndex, 1);
            let serialisedData = objectManager.serializeJSONData(this.existingData);
            let results = fileManager.writeDataIntoFile(serialisedData, objectType);
            if (results.success === true) {
                return connectionHandler.createResponse(results.success, 'Object deleted successfully!');
            }
            else {
                return connectionHandler.createResponse(results.success, results.message);
            }
        } else {
            return connectionHandler.createResponse(false, 'Object Not Found');
        }
    }

    Edit(objectType: any, selectedProperty: string | number, selectedPropertyValue: any, newPropertyValue: any) {
        let results = this.Find(objectType, selectedProperty, selectedPropertyValue);
        let searchResults: any = results.data;
        let matchedObjectIndex = searchResults.requiredObjectIndex;
        if (matchedObjectIndex !== null) {
            this.existingData[matchedObjectIndex][selectedProperty] = newPropertyValue;
            let editedObject = this.existingData[matchedObjectIndex];
            this.existingData.splice(matchedObjectIndex, 1, editedObject);
            let serialisedData = objectManager.serializeJSONData(this.existingData);
            let results = fileManager.writeDataIntoFile(serialisedData, objectType);
            if (results.success === true) {
                return connectionHandler.createResponse(results.success, 'Object edited successfully!');
            }
            else {
                return connectionHandler.createResponse(results.success, results.message);
            }
        } else {
            return connectionHandler.createResponse(false, 'Object Not Found');
        }
    }



}