import { Request } from '../ITT_DB_CommonLibrary/Request';
import { Response } from '../ITT_DB_CommonLibrary/Response';
import * as express from "express";
import { DbManager } from './DbManager';
import { objectManager } from '../ITT_DB_CommonLibrary/ObjectManager';

let db = new DbManager();

export class ConnectionHandler {
    public http: any;
    public io: any;

    createServer() {
        const app = express();
        this.http = require("http").Server(app);
        this.io = require("socket.io")(this.http);
    }

    startServer() {
        this.createServer();
        this.http.listen(3000, function () {
            console.log("listening on *:3000");
        });
    }
    accept() {
        this.io.on("connection", function (socket: any) {
            console.log("connection accepted");
            this.receiveRequest(socket);
        });
    }
    sendResponse(socket: any, message: any) {
        let object = objectManager.serializeJSONData(message);
        socket.emit("message", object);
    }
    receiveRequest(socket: any) {
        socket.on("message", function (message: any) {
            var object = objectManager.deserializeToJSON(message);
            this.processRequest(object, socket);
        });
    }
    async  processRequest(request: Request, socket: any) {
        let type = request.Type;
        let response: Response;
        switch (type) {
            case "save":
                response = await db.Save(request.fileName, request.data);
                this.sendResponse(socket, response);
            case "update":
                response = await db.Edit(request.fileName, request.data.selectedProperty, request.data.selectedPropertyValue, request.data.selectedPropertyNewValue);
                this.sendResponse(socket, response);
            case "find":
                response = await db.Find(request.fileName, request.data.selectedProperty, request.data.selectedPropertyValue);
                this.sendResponse(socket, response);
            case "delete":
                response = await db.Delete(request.fileName, request.data.selectedProperty, request.data.selectedPropertyValue);
                this.sendResponse(socket, response);
            default:
                this.sendResponse(socket, { message: 'Invalid Object', success: false })
                break;
        }
    }
    createResponse(success: boolean, message: string, data?: object) {
        let response: Response;
        response.success = success;
        response.message = message;
        if (data) {
            response.data.requiredObject = data;
        }
        return response;
    }

}

export let connectionHandler = new ConnectionHandler;