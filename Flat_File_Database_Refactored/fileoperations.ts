import fs = require("fs");
import path = require('path');
import { book } from "./BookClass";
import { student } from "./StudentClass";

export class fileoperations {
    public objectdata;
    public existingdata;
    public id = 0;

    Save(objtype) {
        this.readfile(objtype);
        this.getobjectdata(objtype);
        this.objectdata.Id = this.id + 1;
        this.storedata(this.objectdata, objtype);
    }
    readfile(objtype) {
        let filedescriptor = fs.openSync(path.join(process.cwd(), objtype + 'txt'), 'a')
        try {
            let fileContent = fs.readFileSync(objtype + '.txt');
            this.existingdata = JSON.parse(fileContent.toString());

            if (this.existingdata.length !== 0) {
                this.id = this.existingdata[this.existingdata.length - 1].Id;
            }
        }
        catch (err) {
            fs.closeSync(filedescriptor);
            throw new Error(err.message);
        }

    }

    getobjectdata(objtype) {
        switch (objtype) {
            case "student": {
                this.objectdata = student.getstudentdata();
                break;
            }
            case "book": {
                this.objectdata = book.getbookdetails();
                break;
            }
            default: {
                console.log('Invalid input');
                break;
            }
        }
    }

    storedata(objectdata, objtype) {
        let filedescriptor = fs.openSync(path.join(process.cwd(), objtype + 'txt'), 'a')
        this.existingdata.push(objectdata);
        let data = JSON.stringify(this.existingdata);
        try {
            fs.writeFileSync(objtype + '.txt', data);
            console.log('Data stored Successfully');
        }
        catch (err) {
            fs.closeSync(filedescriptor);
            throw new Error(err.message);
        }
    }


    Find(objdetails) {
        this.readfile(objdetails.ObjectType);
        let RequiredObject = this.searchObject(objdetails.selectedproperty, objdetails.selectedpropertyvalue)[0];
        if (RequiredObject instanceof Object == true) {
            this.displayRequiredObjectValues(RequiredObject);
        } else {
            console.log('Object Not Found');
        }
    }

    searchObject(selectedproperty, selectedpropertyvalue) {
        let objIndex = 0;
        let searchresults = [];
        while (objIndex < this.existingdata.length) {
            if (selectedpropertyvalue === this.existingdata[objIndex][selectedproperty]) {
                searchresults[0] = this.existingdata[objIndex];
                searchresults[1] = objIndex;
                return searchresults;
            } else {
                objIndex++;
                if (objIndex == this.existingdata.length) {
                    searchresults[0] = 'Object Not Found';
                    searchresults[1] = '';
                    return searchresults;
                }
            }
        }
    }

    displayRequiredObjectValues(object) {
        let requiredObject = Object.keys(object).map(key => object[key]);
        let requiredObjectDetails = requiredObject.join("\n");
        console.log(requiredObjectDetails);
    }

    Delete(objdetails) {
        this.readfile(objdetails.ObjectType);
        let RequiredObjectIndex = this.searchObject(objdetails.selectedproperty, objdetails.selectedpropertyvalue)[1];
        if (RequiredObjectIndex !== '') {
            this.DeleteObject(objdetails.ObjectType, RequiredObjectIndex);
        } else {
            console.log('Object Not Found');
        }
    }
    DeleteObject(objtype, RequiredObjectIndex) {
        let filedescriptor = fs.openSync(path.join(process.cwd(), objtype + 'txt'), 'a')
        this.existingdata.splice(RequiredObjectIndex, 1);
        let data = JSON.stringify(this.existingdata);
        try {
            fs.writeFileSync(objtype + '.txt', data);
            console.log('Object deleted successfully!');
        }
        catch (err) {
            fs.closeSync(filedescriptor);
            throw new Error(err.message);
        }
    }

    Edit(objdetails) {
        this.readfile(objdetails.ObjectType);
        let RequiredObjectIndex = this.searchObject(objdetails.selectedproperty, objdetails.selectedpropertyvalue)[1];
        if (RequiredObjectIndex !== '') {
            this.EditObject(RequiredObjectIndex, objdetails);
        } else {
            console.log('Object Not Found');
        }
    }
    EditObject(RequiredObjectIndex, objdetails) {
        let filedescriptor = fs.openSync(path.join(process.cwd(), objdetails.objtype + 'txt'), 'a')
        this.existingdata[RequiredObjectIndex][objdetails.selectedproperty] = objdetails.newpropertyvalue;
        let obj = this.existingdata[RequiredObjectIndex];
        this.existingdata.splice(RequiredObjectIndex, 1, obj);
        let data = JSON.stringify(this.existingdata);
        try {
            fs.writeFileSync(objdetails.ObjectType + '.txt', data);
            console.log('Object Edited successfully!');
        }
        catch (err) {
            fs.closeSync(filedescriptor);
            throw new Error(err.message);
        }
    }
}