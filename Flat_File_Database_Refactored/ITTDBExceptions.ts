import fs = require("fs");

export class ITTDBExceptions {
    CheckForEmptyFile(objtype) {
        let existingdata = fs.readFileSync(objtype + '.txt').toString();
        try {
            if (existingdata.length == 0) {
                throw new Error('File is Empty')
            } else {
                return false;
            }
        }
        catch (e) {
            console.log('Error : ' + e.message);
            return true;
        }
    }
    CheckForFileExistance(objtype) {
        try {
            if (!fs.existsSync(objtype + '.txt')) {
                throw new Error('File does not Exists');
            } else {
                return true;
            }
        }
        catch (e) {
            console.log(e.message);
            return false;
        }
    }
}

export let exception = new ITTDBExceptions;