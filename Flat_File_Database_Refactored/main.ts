import read = require("./node_modules/readline-sync");
import { fileoperations } from './fileoperations';
import { book } from "./BookClass";
import { student } from "./StudentClass";
import { exception } from "./ITTDBExceptions";
import { objdetails } from "./ObjectDetail";

let fileoperation = new fileoperations();

function storeDataToDb() {
        let userchoice = read.question("choose the category:\n 1.student\n 2.book\n");
        objdetails.ObjectType = updateobjecttype(userchoice);
        if (exception.CheckForFileExistance(objdetails.ObjectType)) {
            fileoperation.Save(objdetails.ObjectType);
        }
}

function retreiveDataFromDb() {
    let userchoice = read.question("choose the category:\n 1.student\n 2.book\n");
    objdetails.ObjectType = updateobjecttype(userchoice);
    if (!exception.CheckForEmptyFile(objdetails.ObjectType)) {
        objdetails.selectedproperty = selectAndUpdateObjectProperty(objdetails.ObjectType);
        objdetails.selectedpropertyvalue = read.question("Enter the value :\n");
        fileoperation.Find(objdetails);
    }
}

function DeleteDataFromDb() {
    let userchoice = read.question("choose the category:\n 1.student\n 2.book\n");
    objdetails.ObjectType = updateobjecttype(userchoice);
    if (!exception.CheckForEmptyFile(objdetails.ObjectType)) {
        objdetails.selectedproperty = selectAndUpdateObjectProperty(objdetails.ObjectType);
        objdetails.selectedpropertyvalue = read.question("Enter the value :\n");
        fileoperation.Delete(objdetails);
    }
}
function EditDataInDb() {
    let userchoice = read.question("choose the category:\n 1.student\n 2.book\n");
    objdetails.ObjectType = updateobjecttype(userchoice);
    if (!exception.CheckForEmptyFile(objdetails.ObjectType)) {
        objdetails.selectedproperty = selectAndUpdateObjectProperty(objdetails.ObjectType);
        objdetails.selectedpropertyvalue = read.question("Enter the present value :\n");
        objdetails.newpropertyvalue = read.question("Enter new value :\n");
        fileoperation.Edit(objdetails);
    }
}

function selectAndUpdateObjectProperty(objtype) {
    switch (objtype) {
        case "student":
            return student.selectstudentproperty();
        case "book":
            return book.selectbookproperty();
        default:
            console.log("Invalid Object");
            break;
    }
}

function updateobjecttype(userchoice) {
    let objtype;
    switch (userchoice) {
        case "1": objtype = 'student';
            return objtype;
        case "2": objtype = 'book';
            return objtype;
        default: return console.log('Invalid choice');

    }
}

function main() {
    let userChoiceForDbOperation;
    while (1) {
        userChoiceForDbOperation = read.question("Enter the operation to perform:\n1.Store\n2.Retreive\n3.Delete\n4.Edit\n5.Exit\n");
        switch (userChoiceForDbOperation) {
            case "1": storeDataToDb();
                break;
            case "2": retreiveDataFromDb();
                break;
            case "3": DeleteDataFromDb();
                break;
            case "4": EditDataInDb();
                break;
            case "5": return;
            default: console.log("Invalid choice of Operation");
        }
    }

}

main();