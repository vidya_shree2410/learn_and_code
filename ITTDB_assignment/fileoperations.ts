import fs = require("fs");
import { book } from "./BookClass";
import { student } from "./StudentClass";
import {serializer} from "./serialisation";

export class fileOperations {
    public objectData;
    public existingData;
    public id = 0;
    public totalObjects;

    Save(requiredObjectType) {
        if (this.getObjectData(requiredObjectType)) {
            let fileContent = this.readFile(requiredObjectType);
            this.existingData =  serializer.deserializeToJSON(fileContent);
            this.getNextId();
            this.existingData.push(this.objectData);
            let serialisedData = serializer.serializeJSONData(this.existingData);
            this.writeDataIntoFile(serialisedData, requiredObjectType);
        }
    }
    Find(requiredObjectDetails) {
        let objIndex = 0;
        let searchResults = [];
        let fileContent = this.readFile(requiredObjectDetails.type);
        this.existingData =  serializer.deserializeToJSON(fileContent);
        while (objIndex < this.existingData.length) {
            if (requiredObjectDetails.selectedPropertyValue === this.existingData[objIndex][requiredObjectDetails.selectedProperty]) {
                searchResults[0] = this.existingData[objIndex];
                searchResults[1] = objIndex;
                return searchResults;
            } else {
                objIndex++;
                if (objIndex == this.existingData.length) {
                    searchResults[0] = 'Object Not Found';
                    searchResults[1] = '';
                    return searchResults;
                }
            }
        }
    }

    Delete(requiredObjectDetails) {
        let searchResults = this.Find(requiredObjectDetails);
        let matchedObjectIndex = searchResults[1];
        if (matchedObjectIndex !== '') {
            this.existingData.splice(matchedObjectIndex, 1);
            let serialisedData = serializer.serializeJSONData(this.existingData);
            this.writeDataIntoFile(serialisedData, requiredObjectDetails.type);
            console.log('Object deleted successfully!');
        } else {
            console.log('Object Not Found');
        }
    }

    Edit(requiredObjectDetails) {
        let searchResults = this.Find(requiredObjectDetails);
        let matchedObjectIndex = searchResults[1];
        if (matchedObjectIndex !== '') {
            this.existingData[matchedObjectIndex][requiredObjectDetails.selectedProperty] = requiredObjectDetails.newPropertyValue;
            let editedObject = this.existingData[matchedObjectIndex];
            this.existingData.splice(matchedObjectIndex, 1, editedObject);
            let serialisedData = serializer.serializeJSONData(this.existingData);
            this.writeDataIntoFile(serialisedData, requiredObjectDetails.type);
            console.log('Object Edited successfully!');
        } else {
            console.log('Object Not Found');
        }
    }
    readFile(requiredObjectType) {
        let fileContent = fs.readFileSync(requiredObjectType + '.txt');
        return fileContent;
    }
    getNextId() {
        if (this.existingData.length !== 0) {
            this.id = this.existingData[this.existingData.length - 1].Id;
        }
        this.objectData.Id = this.id + 1;
    }

    getObjectData(requiredObjectType) {
        switch (requiredObjectType) {
            case "student": {
                this.objectData = student.getStudentData();
                return true;
            }
            case "book": {
                this.objectData = book.getBookDetails();
                return true;
            }
            default: {
                console.log('Invalid input');
                return false;
            }
        }
    }

    writeDataIntoFile(serialisedData, requiredObjectType) {
        fs.writeFileSync(requiredObjectType + '.txt', serialisedData);
        console.log('Data stored Successfully');
    }
}