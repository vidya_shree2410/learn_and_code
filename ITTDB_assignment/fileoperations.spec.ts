import { Books } from "./BookClass";
import { Students } from "./StudentClass";
import { fileOperations } from './fileoperations';
import { requriedObjectDetails } from "./ObjectDetail"

class MockBooks extends Books {
    getBookDetails = (): object => {
        book.TitleOfTheBook = 'Annana nenapu';
        book.Author = 'Poornachandra Tejaswi';
        book.Language = 'Kannada';
        return book;
    }
}
class MockStudents extends Students {
    getStudentData = (): object => {
        student.Name = 'Vidyashree';
        student.Branch = 'ECE';
        return student;
    }
}
let student = new MockStudents;
let book = new MockBooks;
class Mockfileoperation extends fileOperations {

    getObjectData(requiredObjectType) {
        switch (requiredObjectType) {
            case "student": {
                this.objectData = student.getStudentData();
                return true;
            }
            case "book": {
                this.objectData = book.getBookDetails();
                return true;
            }
            default: {
                console.log('Invalid input');
                return false;
            }
        }
    }
}
let fileoperation = new Mockfileoperation();
describe('fileoperations:Class', () => {
    it('should not save', () => {
        //Arrange
        let requiredObjectType = 'customer';
        //Act
        let results = fileoperation.Save(requiredObjectType);
        //Assert
        expect(results).toBeFalsy();
    })
    it('should save book object', () => {
        //Arrange
        let requiredObjectType = 'book';
        //Act
        let results = fileoperation.Save(requiredObjectType);
        //Assert
        expect(results).toBeTruthy();
    })
    it('should save student object', () => {
        //Arrange
        let requiredObjectType = 'student';
        //Act
        let results = fileoperation.Save(requiredObjectType);
        //Assert
        expect(results).toBeTruthy();
    })
    it('should find student object', () => {
        //Arrange
        requriedObjectDetails.type = 'student';
        requriedObjectDetails.selectedProperty = 'Name';
        requriedObjectDetails.selectedPropertyValue = 'Vidyashree';
        //Act
        let results = fileoperation.Find(requriedObjectDetails);
        //Assert
        expect(results).toBeTruthy();
    })
    it('should not find student object', () => {
        //Arrange
        requriedObjectDetails.type = 'student';
        requriedObjectDetails.selectedProperty = 'Name';
        requriedObjectDetails.selectedPropertyValue = 'Meghana';
        //Act
        let results = fileoperation.Find(requriedObjectDetails);
        //Assert
        expect(results).toBeFalsy();
    })
    it('should delete book object', () => {
        //Arrange
        requriedObjectDetails.type = 'book';
        requriedObjectDetails.selectedProperty = 'TitleOfTheBook';
        requriedObjectDetails.selectedPropertyValue = 'Annana nenapu';
        //Act
        let results = fileoperation.Delete(requriedObjectDetails);
        //Assert
        expect(results).toBeTruthy();
    })

    it('should not delete book object', () => {
        //Arrange
        requriedObjectDetails.type = 'book';
        requriedObjectDetails.selectedProperty = 'TitleOfTheBook';
        requriedObjectDetails.selectedPropertyValue = 'Untethered Soul';
        //Act
        let results = fileoperation.Delete(requriedObjectDetails);
        //Assert
        expect(results).toBeFalsy();
    })
    it('should edit book object', () => {
        //Arrange
        requriedObjectDetails.type = 'book';
        requriedObjectDetails.selectedProperty = 'TitleOfTheBook';
        requriedObjectDetails.selectedPropertyValue = 'Magic of Thinking Big';
        requriedObjectDetails.newPropertyValue = 'The Magic of Thinking Big';
        //Act
        let results = fileoperation.Edit(requriedObjectDetails);
        //Assert
        expect(results).toBeTruthy();
    })
    it('should not edit book object', () => {
        //Arrange
        requriedObjectDetails.type = 'book';
        requriedObjectDetails.selectedProperty = 'TitleOfTheBook';
        requriedObjectDetails.selectedPropertyValue = 'Magic Book';
        requriedObjectDetails.newPropertyValue = 'The Magic of Thinking Big';
        //Act
        let results = fileoperation.Find(requriedObjectDetails);
        //Assert
        expect(results).toBeFalsy();
    })

})