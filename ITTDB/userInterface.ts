import read = require("readline-sync");
import { DbManager } from './DbManager';
import { book } from "./BookClass";
import { student } from "./StudentClass";
import { fileManager } from "./FileManager";

let db = new DbManager();
let objectDetails = {};

function storeDataToDB() {
    let userChoice = read.question("choose the category:\n 1.student\n 2.book\n");
    let objectType = updateObjectType(userChoice);
    console.log(objectType);
    if (getObjectData(objectType)) {
        if (fileManager.checkForFileExistance(objectType)) {
            db.Save(objectType, objectDetails);
        }
    }
}

function getDataFromDB() {
    let userChoice = read.question("choose the category:\n 1.student\n 2.book\n");
    let objectType = updateObjectType(userChoice);
    if (!fileManager.checkForEmptyFile(objectType)) {
        let selectedProperty = selectAndUpdateObjectProperty(objectType);
        let selectedPropertyValue = read.question("Enter the value :\n");
        let searchResults = db.Find(objectType, selectedProperty, selectedPropertyValue);
        let matchedObject = searchResults[0];
        if (matchedObject instanceof Object == true) {
            displayRequiredObjectValues(matchedObject);
            return true;
        } else {
            console.log('Object Not Found');
            return false;
        }
    }
}

function deleteDataFromDB() {
    let userChoice = read.question("choose the category:\n 1.student\n 2.book\n");
    let objectType = updateObjectType(userChoice);
    if (!fileManager.checkForEmptyFile(objectType)) {
        let selectedProperty = selectAndUpdateObjectProperty(objectType);
        let selectedPropertyValue = read.question("Enter the value :\n");
        db.Delete(objectType, selectedProperty, selectedPropertyValue);
    }
}
function editDataInDB() {
    let userChoice = read.question("choose the category:\n 1.student\n 2.book\n");
    let objectType = updateObjectType(userChoice);
    if (!fileManager.checkForEmptyFile(objectType)) {
        let selectedProperty = selectAndUpdateObjectProperty(objectType);
        let selectedPropertyValue = read.question("Enter the present value :\n");
        let newPropertyValue = read.question("Enter new value :\n");
        db.Edit(objectType, selectedProperty, selectedPropertyValue, newPropertyValue);
    }
}

function selectAndUpdateObjectProperty(objectType) {
    switch (objectType) {
        case "student":
            return student.selectStudentProperty();
        case "book":
            return book.selectBookProperty();
        default:
            console.log("Invalid Object");
            break;
    }
}

function getObjectData(objectType) {
    switch (objectType) {
        case "student": {
            objectDetails = student.getStudentData();
            return true;
        }
        case "book": {
            objectDetails = book.getBookDetails();
            return true;
        }
        default: {
            console.log('Invalid input');
            return false;
        }
    }
}

function updateObjectType(objectType) {
    let requiredObjectType;
    switch (objectType) {
        case "1": requiredObjectType = 'student';
            return requiredObjectType;
        case "2": requiredObjectType = 'book';
            return requiredObjectType;
        default: return console.log('Invalid choice');

    }
}

function displayRequiredObjectValues(object) {
    let requiredObject = Object.keys(object).map(key => object[key]);
    let requiredObjectDetails = requiredObject.join("\n");
    console.log(requiredObjectDetails);
}

function main() {
    let userChoiceForDbOperation;
    while (1) {
        userChoiceForDbOperation = read.question("Enter the operation to perform:\n1.Store\n2.Retreive\n3.Delete\n4.Edit\n5.Exit\n");
        switch (userChoiceForDbOperation) {
            case "1": storeDataToDB();
                break;
            case "2": getDataFromDB();
                break;
            case "3": deleteDataFromDB();
                break;
            case "4": editDataInDB();
                break;
            case "5": return;
            default: console.log("Invalid choice of Operation");
        }
    }

}

main();