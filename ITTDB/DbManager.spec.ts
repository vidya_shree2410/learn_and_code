import { DbManager } from './DbManager';

let dbManager = new DbManager();

describe('DbManager:Class', () => {
    it('should save', () => {
        //Arrange
        let objectType = 'student';
        let objectData = {
            "Name":"Vidya",
            "Branch":"EC"
        }
        //Act
        let results = dbManager.Save(objectType, objectData);
        //Assert
        expect(results).toBeUndefined();
    })
    it('should not find the required object', () => {
        //Arrange
        let objectType ="student";
        let selectedProperty ="Name";
        let selectedPropertyValue = "Kavya";
        //Act
        let objectFound = dbManager.Find(objectType, selectedProperty, selectedPropertyValue)[1];
       // Assert
        expect(objectFound).toEqual('');
    })
    it('should edit the required object', () => {
        //Arrange
        let objectType ="student";
        let selectedProperty ="Name";
        let selectedPropertyValue = "Vidya";
        let newPropertyValue = "Vidyashree";
        //Act
        let results = dbManager.Edit(objectType, selectedProperty, selectedPropertyValue, newPropertyValue);
       // Assert
        expect(results).toBeUndefined();
    })
    it('should not edit the required object', () => {
        //Arrange
        let objectType ="student";
        let selectedProperty ="Name";
        let selectedPropertyValue = "Kavya";
        let newPropertyValue = "Vidyashree";
        //Act
        let results = dbManager.Edit(objectType, selectedProperty, selectedPropertyValue, newPropertyValue);
       // Assert
        expect(results).toBeUndefined();
    })
    it('should not delete the required object', () => {
        //Arrange
        let objectType ="student";
        let selectedProperty ="Name";
        let selectedPropertyValue = "Kavya";
        //Act
        let results = dbManager.Delete(objectType, selectedProperty, selectedPropertyValue);
       // Assert
        expect(results).toBeUndefined();
    })
    it('should delete the required object', () => {
        //Arrange
        let objectType ="student";
        let selectedProperty ="Name";
        let selectedPropertyValue = "Vidyashree";
        //Act
        let results = dbManager.Delete(objectType, selectedProperty, selectedPropertyValue);
       // Assert
        expect(results).toBeUndefined();
    })
})