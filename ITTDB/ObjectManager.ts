
export class ObjectManager {
    serializeJSONData(data) {
        let serialisedData = JSON.stringify(data);
        return serialisedData;
    }
    deserializeToJSON(fileContent) {
        let deserializedData = JSON.parse(fileContent.toString());
        return deserializedData;
    }
}
export let objectManager = new ObjectManager();