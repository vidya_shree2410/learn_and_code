import fs = require("fs");
import { objectManager } from "./ObjectManager";

export class FileManager {
    public id = 0;
    public existingData;

    readFile(requiredObjectType) {
        let fileContent = fs.readFileSync(requiredObjectType + '.txt');
        return fileContent;
    }

    writeDataIntoFile(serialisedData, requiredObjectType) {
        fs.writeFileSync(requiredObjectType + '.txt', serialisedData);
        console.log('Data stored Successfully');
    }

    getData(objectType) {
        let fileContent = fileManager.readFile(objectType);
        this.existingData = objectManager.deserializeToJSON(fileContent);
        return this.existingData;
    }

    getNextId() {
        if (this.existingData.length !== 0) {
            this.id = this.existingData[this.existingData.length - 1].id;
        }
        return this.id + 1;
    }

    checkForEmptyFile(requiredObjectType) {
        let existingData = fs.readFileSync(requiredObjectType + '.txt').toString();
        try {
            if (existingData.length == 0) {
                throw new Error('File is Empty')
            } else {
                return false;
            }
        }
        catch (e) {
            console.log('Error : ' + e.message);
            return true;
        }
    }
    
    checkForFileExistance(requiredObjectType) {
        try {
            if (!fs.existsSync(requiredObjectType + '.txt')) {
                throw new Error('File does not Exists');
            } else {
                return true;
            }
        }
        catch (e) {
            console.log(e.message);
            return false;
        }
    }
}

export let fileManager = new FileManager;