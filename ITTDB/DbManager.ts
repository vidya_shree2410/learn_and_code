import { objectManager } from "./ObjectManager";
import { fileManager } from "./FileManager";

export class DbManager {
    public objectData;
    public existingData;
    public id = 0;

    Save(objectType, objectData) {
        this.existingData = fileManager.getData(objectType);
        this.objectData = objectData;
        this.objectData.id = fileManager.getNextId();
        this.existingData.push(this.objectData);
        let serialisedData = objectManager.serializeJSONData(this.existingData);
        fileManager.writeDataIntoFile(serialisedData, objectType);
    }
    Find(objectType, selectedProperty, selectedPropertyValue) {
        this.existingData = fileManager.getData(objectType);
        let objIndex = 0;
        let searchResults = [];
        while (objIndex < this.existingData.length) {
            if (selectedPropertyValue === this.existingData[objIndex][selectedProperty]) {
                searchResults[0] = this.existingData[objIndex];
                searchResults[1] = objIndex;
                return searchResults;
            } else {
                objIndex++;
                if (objIndex == this.existingData.length) {
                    searchResults[0] = 'Object Not Found';
                    searchResults[1] = '';
                    return searchResults;
                }
            }
        }
    }

    Delete(objectType, selectedProperty, selectedPropertyValue) {
        let searchResults = this.Find(objectType, selectedProperty, selectedPropertyValue);
        let matchedObjectIndex = searchResults[1];
        if (matchedObjectIndex !== '') {
            this.existingData.splice(matchedObjectIndex, 1);
            let serialisedData = objectManager.serializeJSONData(this.existingData);
            fileManager.writeDataIntoFile(serialisedData, objectType);
            console.log('Object deleted successfully!');
        } else {
            console.log('Object Not Found');
        }
    }

    Edit(objectType, selectedProperty, selectedPropertyValue, newPropertyValue) {
        let searchResults = this.Find(objectType, selectedProperty, selectedPropertyValue);
        let matchedObjectIndex = searchResults[1];
        if (matchedObjectIndex !== '') {
            this.existingData[matchedObjectIndex][selectedProperty] = newPropertyValue;
            let editedObject = this.existingData[matchedObjectIndex];
            this.existingData.splice(matchedObjectIndex, 1, editedObject);
            let serialisedData = objectManager.serializeJSONData(this.existingData);
            fileManager.writeDataIntoFile(serialisedData, objectType);
            console.log('Object Edited successfully!');
        } else {
            console.log('Object Not Found');
        }
    }



}