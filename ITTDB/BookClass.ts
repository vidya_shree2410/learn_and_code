import read = require("./node_modules/readline-sync");

export class Books {
    Id: number;
    TitleOfTheBook: string;
    Author: string;
    Language: string;
    getBookDetails: () => object;
    selectBookProperty: () => string;
}

export let book = new Books;

Books.prototype.getBookDetails = (): object => {
    book.TitleOfTheBook = read.question('Enter title of the book: ');
    book.Author = read.question('Enter author name: ');
    book.Language = read.question('Enter language: ');
    return book;
}

Books.prototype.selectBookProperty = (): string => {
    let selectedoption = read.question('Please select the property :\n 1.TitleOfTheBook \n 2.Author \n 3.Language \n');
    switch (selectedoption) {
        case "1": return 'TitleOfTheBook';
        case "2": return 'Author';
        case "3": return 'Language';
        default:
            console.log("Invalid property selection");
            return;
    }
}